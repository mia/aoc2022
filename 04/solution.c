#include <stdio.h>
#include <ctype.h>

int main(void) {
    int a, b, c, d, S = 0, G = 0;
    char l[64], *e;

    while (fgets(l, 64, stdin)) {
        a = b = c = d = 0;
        e = l;
#define rdec(V) { while (isdigit(*e)) V = 10 * V + (*e++ - '0'); e++; }
        rdec(a) rdec(b) rdec(c) rdec(d)
        S += (a >= c && b <= d) || (c >= a && d <= b);
        G += (a <= c && b >= c) || (c <= a && d >= a);
    }

    printf("Silver: %d\nGold: %d\n", S, G);
}

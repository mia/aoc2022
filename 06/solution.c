#include <stdio.h>
#include <string.h>

int count(const char *l, size_t len, int num) {
    int i, j, cc[26] = {0}, tc;

    for (i = 0; i < len - num; i++) {
        if (i > 0) {
            cc[l[i-1] - 'a']--;
            cc[l[i+num-1] - 'a']++;
        } else
            for (j = 0; j < num; j++)
                cc[l[i+j] - 'a']++;

        for (j = 0, tc = 0; j < 26; j++)
            tc += cc[j] == 1;

        if (tc == num)
            return i + num;
    }

    return 0;
}

int main(void) {
    char l[4096];
    size_t len;

    fgets(l, 4096, stdin);
    len = strlen(l);

    printf("Silver: %d\nGold: %d\n",
           count(l, len, 4), count(l, len, 14));
}

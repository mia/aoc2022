#include <stdio.h>

int main(void) {
    char insn[5];
    int arg = 0, X = 1, clock = 1, S = 0;

    printf("Gold:\n\n\t");

    while (scanf("%s %i\n", insn, &arg) > 0) {
        for (int busy = *insn == 'a' ? 2 : 1; busy > 0; busy--, clock++) {
            S += !((clock - 20) % 40) * clock * X;

            printf("%s%s", ((clock - 1) % 40 >= X - 1 &&
                            (clock - 1) % 40 <= X + 1) ? "█" : "░",
                   (clock % 40) ? "" : "\n\t");
        }

        X += (*insn == 'a') * arg;
    }

    printf("\nSilver: %i\n", S);
}

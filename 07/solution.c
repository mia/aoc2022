#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

typedef struct entry {
    size_t size;
    size_t n_entries;
    size_t n_alloc;
    size_t visit;
    struct entry **entries;
    struct entry *parent;
} entry;

entry *addent(entry *parent) {
    if (parent->n_entries >= parent->n_alloc) {
        parent->n_alloc += 5;
        if (parent->n_entries)
            parent->entries = realloc(parent->entries, parent->n_alloc * sizeof(entry*));
        else
            parent->entries = malloc(parent->n_alloc * sizeof(entry*));
    }
    parent->n_entries++;

    entry *e = parent->entries[parent->n_entries - 1] = calloc(1, sizeof(entry));
    e->parent = parent;

    return e;
}

int main(int argc, char **argv) {
    if (argc < 2) return 1;

    entry *root = calloc(1, sizeof(entry));
    entry *current = root;
    size_t S = 0, G = 0, required = 0;

    struct stat s;
    if (stat(argv[1], &s) == -1) return 1;
    int fd = open(argv[1], O_RDONLY);
    if (fd == -1) return 1;

    char *in = mmap(NULL, s.st_size, PROT_READ, MAP_SHARED, fd, 0);
    if (in == MAP_FAILED) return 1;
    char *l = in;

    while (l != in + s.st_size) {
        char *nl = strchr(l, '\n');
        if (l[0] == 'd') {
            addent(current);
        } else if (l[0] == '$') {
            if (l[2] == 'c') {
                if (l[5] == '/')
                    current = root;
                else if (l[5] == '.') {
                    if (current->parent != root)
                        current->parent->size += current->size;
                    current = current->parent;
                } else
                    current = current->entries[current->visit++];
            }
        } else {
            size_t sz = 0;
            do { sz = 10 * sz + (*l++ - '0'); } while (*l != ' ');
            current->size += sz;
        }
        l = nl + 1;
    }

    for (size_t i = 0; i < root->n_entries; i++)
        root->size += root->entries[i]->size;

    if (argc == 4)
        required = strtoul(argv[3], NULL, 10) - (strtoul(argv[2], NULL, 10) - root->size);
    else
        required = 30000000UL - (70000000UL - root->size);

    current = root;
    do {
        if (current->n_entries) {
            current->n_entries--;
            current = (current->entries++)[0];
        } else {
            if (current->size < 100000)
                S += current->size;
            else if (current->size >= required && (current->size < G || !G))
                G = current->size;

            current = current->parent;
        }
    } while (current);

    printf("Silver: %lu\nGold: %lu\n", S, G);
}

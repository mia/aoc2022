#!/usr/bin/env python3

import sys

res = sorted(sum(map(int, l.split("\n"))) for l in sys.stdin.read().strip().split("\n\n"))[-3:]
print(f"Silver: {res[-1]}\nGold: {sum(res)}")

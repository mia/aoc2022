#include <stdio.h>
#include <stdlib.h>

int main(void) {
    char l[8];
    int c, i, t, S, G = 0, E[256] = {0}, e = 0;

    while (fgets(l, 8, stdin)) {
        E[e] += c = atoi(l);
        if (!c) e++;
    }

    for (i = 0; i < 3; i++, t = 0) {
        for (c = 0; c <= e; c++) if (E[c] > E[t]) t = c;
        G += E[t];
        E[t] = 0;
        if (!i) S = G;
    }

    printf("Silver: %d\nGold: %d\n", S, G);
}

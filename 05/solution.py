#!/usr/bin/env python3

import sys
import re
from collections import defaultdict

insr = re.compile(r"move (\d+) from (\d+) to (\d+)")

stacksA = defaultdict(bytearray)
stacksB = defaultdict(bytearray)

for l in sys.stdin:
    if "[" in l:
        for i, c in enumerate(l[1::4], 1):
            if c != ' ':
                c = ord(c)
                stacksA[i].insert(0, c)
                stacksB[i].insert(0, c)

    im = insr.match(l)
    if im:
        n, f, t = map(int, im.groups())
        stacksA[t] += stacksA[f][-n:][::-1]
        stacksA[f] = stacksA[f][:-n]
        stacksB[t] += stacksB[f][-n:]
        stacksB[f] = stacksB[f][:-n]

print(f"""Silver: {"".join(chr(stacksA[i][-1]) for i in sorted(stacksA))}
Gold: {"".join(chr(stacksB[i][-1]) for i in sorted(stacksB))}""")

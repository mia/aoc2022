#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct crates {
    char *c;
    size_t len, alloc;
} crates;

#define alloc_stacks(C)                          \
    if (C) C = realloc(C, sizeof(crates) * A);   \
    else C = malloc(sizeof(crates) * A);         \
    for (i = N; i < A; i++) C[i].c = 0;

#define alloc_crates(C, N)                       \
    if (C.c) {                                   \
        C.len += N;                              \
        if (C.len > C.alloc)                     \
            C.c = realloc(C.c, C.alloc = C.len); \
    } else                                       \
        C.c = malloc(C.len = C.alloc = N);

#define crate_pos(C, N) (C.c + C.len + N)

int main(void) {
    char l[BUFSIZ], *e, *m = 0, ic, *tc, *tf;
    crates *stacksA = 0, *stacksB = 0;
    int i, n, N = 0, A, f, t;

    while (fgets(l, BUFSIZ, stdin)) {
        if (l[0] == 'm') {
            n = strtol(l + 5, &e, 10);
            f = strtol(e + 6, &e, 10) - 1;
            t = atoi(e + 4) - 1;

            alloc_crates(stacksA[t], n)
            tf = crate_pos(stacksA[f], -1);
            tc = crate_pos(stacksA[t], -n);
            for (i = 0; i < n; i++)
                tc[i] = tf[-i];
            stacksA[f].len -= n;

            alloc_crates(stacksB[t], n)
            tf = crate_pos(stacksB[f], -n);
            tc = crate_pos(stacksB[t], -n);
            for (i = 0; i < n; i++)
                tc[i] = tf[i];
            stacksB[f].len -= n;
        } else if ((e = strrchr(l, ']'))) {
            if (e > m) {
                m = e;
                A = (m - l) / 4 + 1;
                alloc_stacks(stacksA)
                alloc_stacks(stacksB)
                N = A;
            }

            for (i = 0; i < N; i++) {
                if ((ic = l[i*4+1]) != ' ') {
                    alloc_crates(stacksA[i], 1)
                    alloc_crates(stacksB[i], 1)
                    memmove(stacksA[i].c + 1, stacksA[i].c, stacksA[i].len - 1);
                    memmove(stacksB[i].c + 1, stacksB[i].c, stacksB[i].len - 1);
                    stacksA[i].c[0] = ic;
                    stacksB[i].c[0] = ic;
                }
            }
        }
    }

    printf("Silver: ");
    for (i = 0; i < N; i++)
        printf("%c", *crate_pos(stacksA[i], -1));
    printf("\nGold: ");
    for (i = 0; i < N; i++)
        printf("%c", *crate_pos(stacksB[i], -1));
    printf("\n");
}

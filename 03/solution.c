#include <stdio.h>
#include <string.h>

#define P(c) (c < 97 ? c - 38 : c - 96)

int main(void) {
    char l[128];
    int e = 0, i, n, S = 0, G = 0, g[53] = {0};

    while (fgets(l, 128, stdin)) {
        n = strlen(l) - 1;

        int s[53] = {0};
        for (i = 0; i < n / 2; i++) s[P(l[i])] = 1;
        for (i = n / 2; i < n; i++) s[P(l[i])] *= 2;
        for (i = 1; i < 53; i++) if (s[i] > 1) S += i;

        if (!(e % 3))
            for (i = 0; i < n; i++) g[P(l[i])] = 1;
        else {
            for (i = 0; i < n; i++) g[P(l[i])] *= 2;
            for (i = 1; i < 53; i++) g[i] = g[i] > 1;
        }

        if (!(++e % 3)) {
            for (i = 1; i < 53; i++) if (g[i]) G += i;
            memset(g, 0, 53 * sizeof(int));
        }
    }

    printf("Silver: %d\nGold: %d\n", S, G);
}

#!/usr/bin/env python3

import sys

S = G = 0
for l in zip(*[iter(list(map(lambda c: c - 38 if c < 97 else c - 96, map(ord, x.strip()))) for x in sys.stdin)] * 3):
    G += set.intersection(*map(set, l)).pop()
    S += sum(set(x[:len(x) // 2]).intersection(x[len(x) // 2:]).pop() for x in l)

print(f"Silver: {S}\nGold: {G}")

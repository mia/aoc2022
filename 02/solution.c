#include <stdio.h>

int main(void) {
    char e, p;
    int s = 0, g = 0;

    while(scanf("%c %c\n", &e, &p) == 2) {
        e -= '@';
        p -= 'W';
        s += p + (p == e + 1 || p == e - 2 ? 2 : p == e) * 3;
        g += (p - 1) * 3 + (p == 3 ? e > 2 ? 1 : e + 1 : p == 2 ? e : e < 2 ? 3 : e - 1);
    }

    printf("Silver: %d\nGold: %d\n", s, g);
}
